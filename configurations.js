module.exports = {

	'TIMEOUT' : 5000,
	'APIVERSION' : '1.0',
	'DEBUG': 'false',

	'AUTH':{
		'TYPE': 'basic',
		'USERNAME': 'advantteam',
		'PASSWORD': 'sm@rtdev',
	},

	'SERVER':{
		'protocol':'https',
		'host':'api.bitbucket.org',
		'repositorypath':'/advantteam/ciserver'
	},
	

	'SCHEDULER':{

		'TASKCOUNT':1,
		'INTERVAL':3600000,	

		'DAILY':{
		'dayofweek':'0',
		'hour':'23',
		'minute':'0'
		},

		'RECURRING':{
			'dayofweek':'0',
			'hour':'0',
			'minute': '5'
		}

	}
}

//module.exports = config;